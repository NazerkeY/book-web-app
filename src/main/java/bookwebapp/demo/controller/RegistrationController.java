package bookwebapp.demo.controller;

import bookwebapp.demo.converter.UserConverter;
import bookwebapp.demo.dto.UserRegistrationDTO;
import bookwebapp.demo.model.Response;
import bookwebapp.demo.model.Role;
import bookwebapp.demo.model.User;
import bookwebapp.demo.repository.UserRepository;
import bookwebapp.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;

@RestController
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private UserConverter userConverter;
    private Responses responses;

    @RequestMapping(value = "/api/signup", method = RequestMethod.GET)
    public String signUp(){
        return "signUp";
    }

    @RequestMapping(value = "/api/signup", method = RequestMethod.POST)
    public UserRegistrationDTO signUp(@Valid @RequestBody UserRegistrationDTO userRegistrationDTO){
        if(!userService.checkUser(userRegistrationDTO.getEmail())){
            if(userRegistrationDTO.getPassword().equals(userRegistrationDTO.getConfirmPassword())){
                User user = userConverter.dtoToEntity(userRegistrationDTO);
                user.setPassword(bCryptPasswordEncoder.encode(userRegistrationDTO.getPassword()));
                user.setRoles(Collections.singleton(new Role("ROLE_USER")));
                user = userRepository.save(user);
                return userConverter.entityToDto(user);
            }
            else{
                responses.passwordConflict();
               return null;
            }
        }
        else{
            responses.userExists();
            return null;
        }
    }
}
