package bookwebapp.demo.controller;

import bookwebapp.demo.converter.UserConverter;
import bookwebapp.demo.dto.UserRegistrationDTO;
import bookwebapp.demo.model.User;
import bookwebapp.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AdminController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserConverter userConverter;

    @RequestMapping(value = "/api/admin/users", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserRegistrationDTO> getUsers() {
        List<User> users = userRepository.findAll();
        return userConverter.entityToDto(users);
    }
}
