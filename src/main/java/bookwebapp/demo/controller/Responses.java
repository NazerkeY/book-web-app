package bookwebapp.demo.controller;

import bookwebapp.demo.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Responses {
    public ResponseEntity<?> passwordConflict(){
        return new ResponseEntity<>(new Response("Passwords don't match!"), HttpStatus.CONFLICT);
    }

    public ResponseEntity<?> userExists(){
        return new ResponseEntity<>(new Response("User already exist!"), HttpStatus.ALREADY_REPORTED);
    }
}
