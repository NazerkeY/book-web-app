package bookwebapp.demo.controller;

import bookwebapp.demo.converter.GenreConverter;
import bookwebapp.demo.dto.GenreDTO;
import bookwebapp.demo.dto.UserRegistrationDTO;
import bookwebapp.demo.model.Genre;
import bookwebapp.demo.repository.BookRepository;
import bookwebapp.demo.repository.GenreRepository;
import bookwebapp.demo.service.BookService;
import bookwebapp.demo.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BookController {
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    BookService bookService;
    @Autowired
    GenreService genreService;
    @Autowired
    GenreConverter genreConverter;

    @RequestMapping(value = "/api/books", method = RequestMethod.GET)
    public List<Genre> getGenres() {
        List<Genre> genres = genreRepository.findAll();
        return genres;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/api/books", method = RequestMethod.POST)
    public GenreDTO addGenre(@Valid @RequestBody GenreDTO genreDTO){
        if(!genreService.checkGenre(genreDTO.getName())){
            Genre genre = genreConverter.dtoToEntity(genreDTO);
            genre = genreRepository.findByName(genreDTO.getName());
            return genreConverter.entityToDto(genre);
        }
        return null;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/api/genre/{id}")
    Genre updateGenre(@RequestBody Genre newGenre, @PathVariable Long id) {

        return genreRepository.findById(id)
                .map(genre -> {
                    genre.setName(newGenre.getName());
                    return genreRepository.save(genre);
                })
                .orElseGet(() -> {
                    newGenre.setId(id);
                    return genreRepository.save(newGenre);
                });
    }
}
