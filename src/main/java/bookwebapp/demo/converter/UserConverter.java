package bookwebapp.demo.converter;

import bookwebapp.demo.dto.UserRegistrationDTO;
import bookwebapp.demo.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserConverter {

    public UserRegistrationDTO entityToDto(User user){
        ModelMapper modelMapper = new ModelMapper();
        UserRegistrationDTO map = modelMapper.map(user, UserRegistrationDTO.class);
        return map;
    }

    public List<UserRegistrationDTO> entityToDto(List<User> userList){
        return userList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public User dtoToEntity(UserRegistrationDTO userRegistrationDTO){
        ModelMapper modelMapper = new ModelMapper();
        User map = modelMapper.map(userRegistrationDTO, User.class);
        return map;
    }
    public List<User> dtoToEntity(List<UserRegistrationDTO> userRegistrationDTOList){
        return userRegistrationDTOList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
