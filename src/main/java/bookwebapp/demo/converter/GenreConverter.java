package bookwebapp.demo.converter;

import bookwebapp.demo.dto.BookDTO;
import bookwebapp.demo.dto.GenreDTO;
import bookwebapp.demo.model.Book;
import bookwebapp.demo.model.Genre;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GenreConverter {
    public GenreDTO entityToDto(Genre genre){
        ModelMapper modelMapper = new ModelMapper();
        GenreDTO map = modelMapper.map(genre, GenreDTO.class);
        return map;
    }

    public List<GenreDTO> entityToDto(List<Genre> genreList){
        return genreList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Genre dtoToEntity(GenreDTO genreDTO){
        ModelMapper modelMapper = new ModelMapper();
        Genre map = modelMapper.map(genreDTO, Genre.class);
        return map;
    }
    public List<Genre> dtoToEntity(List<GenreDTO> genreDTOList){
        return genreDTOList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
