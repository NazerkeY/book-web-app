package bookwebapp.demo.converter;

import bookwebapp.demo.dto.BookDTO;
import bookwebapp.demo.model.Book;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookConverter {

    public BookDTO entityToDto(Book book){
        ModelMapper modelMapper = new ModelMapper();
        BookDTO map = modelMapper.map(book, BookDTO.class);
        return map;
    }

    public List<BookDTO> entityToDto(List<Book> bookList){
        return bookList.stream().map(x -> entityToDto(x)).collect(Collectors.toList());
    }

    public Book dtoToEntity(BookDTO bookDTO){
        ModelMapper modelMapper = new ModelMapper();
        Book map = modelMapper.map(bookDTO, Book.class);
        return map;
    }
    public List<Book> dtoToEntity(List<BookDTO> bookDTOList){
        return bookDTOList.stream().map(x -> dtoToEntity(x)).collect(Collectors.toList());
    }
}
