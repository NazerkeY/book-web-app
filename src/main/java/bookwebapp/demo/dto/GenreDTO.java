package bookwebapp.demo.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class GenreDTO {

    @NotEmpty(message = "name is required")
    @Size(min = 6, max = 30, message = "Name of genre must be between 6 and 30 characters")
    private String name;

    public GenreDTO(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
