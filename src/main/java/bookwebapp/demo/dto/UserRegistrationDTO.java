package bookwebapp.demo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRegistrationDTO {

    @Email(message = "Email should be valid")
    @NotEmpty(message = "email is required")
    private String email;

    @NotEmpty(message = "username is required")
    @Size(min = 6, max = 30, message = "Username must be between 6 and 30 characters")
    private String username;

    @NotEmpty(message = "password is required")
    @Size(min = 7, message = "Password shouldn't be less than 8")
    private String password;

    @NotEmpty(message = "password is required")
    @Size(min = 7, message = "Password shouldn't be less than 8")
    private String confirmPassword;


    public UserRegistrationDTO(){}

    public UserRegistrationDTO(String email, String username, String password, String confirmPassword){
        this.email = email;
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
