package bookwebapp.demo.dto;

import bookwebapp.demo.model.Genre;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

public class BookDTO {

    @Size(min = 3, max = 50, message = "Title must be between 3 and 50 characters")
    @NotEmpty(message = "title is required")
    private String title;

    @NotEmpty(message = "username is required")
    @Size(min = 3, max = 50, message = "Name of author must be between 3 and 50 characters")
    private String author;

    @NotEmpty(message = "description is required")
    @Size(min = 10, max = 200, message = "Description must be between 10 and 200 characters")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private Set<Genre> genre;

    public BookDTO(String title, String author, String description, Set<Genre> genre) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.genre = genre;
    }

    public BookDTO() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Genre> getGenre() {
        return genre;
    }

    public void setGenre(Set<Genre> genre) {
        this.genre = genre;
    }
}
