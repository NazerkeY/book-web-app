package bookwebapp.demo.service;

import bookwebapp.demo.dto.BookDTO;
import bookwebapp.demo.model.Book;
import bookwebapp.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;

    public boolean checkBook(String title, String author){
        Book bookFromData = bookRepository.findByTitleAndAuthor(title, author);
        if(bookFromData != null){
            return true;
        }
        else{
            return false;
        }
    }

    public Book save(BookDTO bookDTO){
        Book book = new Book(bookDTO.getTitle(), bookDTO.getAuthor(), bookDTO.getDescription(), bookDTO.getGenre());
        return bookRepository.save(book);
    }
}
