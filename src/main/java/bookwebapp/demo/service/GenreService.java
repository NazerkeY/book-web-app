package bookwebapp.demo.service;

import bookwebapp.demo.dto.GenreDTO;
import bookwebapp.demo.model.Genre;
import bookwebapp.demo.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreService {
    @Autowired
    GenreRepository genreRepository;

    public boolean checkGenre(String name){
        Genre genreFromData = genreRepository.findByName(name);
        if(genreFromData != null){
            return true;
        }
        else{
            return false;
        }
    }

    public Genre save(GenreDTO genreDTO){
        Genre genre = new Genre(genreDTO.getName());
        return genreRepository.save(genre);
    }
}
