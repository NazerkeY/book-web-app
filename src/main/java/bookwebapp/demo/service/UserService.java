package bookwebapp.demo.service;

import bookwebapp.demo.dto.UserRegistrationDTO;
import bookwebapp.demo.model.Role;
import bookwebapp.demo.model.User;
import bookwebapp.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if(user != null){
            return user;
        }
        else{
            throw new UsernameNotFoundException(String.format("User with %s email cannot be found", email));
        }
    }

    public boolean checkUser(String email){
        User userFromData = userRepository.findByEmail(email);
        if(userFromData != null){
            return true;
        }
        else{
            return false;
        }
    }

    public User save(UserRegistrationDTO userRegistrationDTO){
        User user = new User(userRegistrationDTO.getEmail(), userRegistrationDTO.getUsername(), bCryptPasswordEncoder.encode(userRegistrationDTO.getPassword()));
        user.setRoles(Collections.singleton(new Role("ROLE_USER")));
        return userRepository.save(user);
    }
}
